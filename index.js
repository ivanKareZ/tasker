const electron = require("electron");
const url = require("url");
const path = require("path");
const constants = require("./constants");

const { app, BrowserWindow, Menu } = electron;

function getTemplateURL(templateName) {
    return url.format({
        pathname: getTemplatePath(templateName),
        protocol: 'file',
        slashes: true
    });
}

function getTemplatePath(templateName) {
    return path.join(__dirname, 'html', templateName + '.html');
}

app.on('ready', function () {
    const mainWindow = new BrowserWindow({});
    mainWindow.loadURL(getTemplateURL('mainWindow'));
    mainWindow.setMenu(null);
});